void main() {
  var appdetails = App();
  appdetails.appname = "Ambani Africa";
  appdetails.appcategory = "EducationTechnology";
  appdetails.appdeveloper = "Ambani Africa";
  appdetails.winningyear = 2021;

  print("The name of the app is " + (appdetails.appname).toString() + ".");
  print("The category of the app is " +
      (appdetails.appcategory).toString() +
      ".");
  print("The developer of the app is " +
      (appdetails.appdeveloper).toString() +
      ".");
  print("The app won in " + (appdetails.winningyear).toString() + ".");
  print("");
  appdetails.tranform();
}

class App {
  String? appname;
  String? appcategory;
  String? appdeveloper;
  int? winningyear;

  void tranform() {
    print("The name of the app is " + ((appname).toString()).toUpperCase());
  }
}
