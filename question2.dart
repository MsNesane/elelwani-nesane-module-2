void main() {
  var winningapps = <String>[
    "FNB",
    "SnapScan",
    "LIVE Inspect",
    "Wum Drop",
    "Domestly",
    "Shyft",
    "Khula Ecosystem",
    "Naked Insurance",
    "Easy Equities",
    "Ambani Africa"
  ];

  winningapps.sort((String left, String right) {
    return left.compareTo(right);
  });

  for (var name in winningapps) {
    print(name);
  }

  print("The #MTNAppOfTheYear winning apps for 2017 and 2018 are " +
      winningapps[7] +
      " and " +
      winningapps[4] +
      " respectively.");

  print("The total number of #MTNAppOfTheYear winning apps in the array is " +
      (winningapps.length).toString() +
      ".");
}
